from threading import Thread
import json
from PIL import Image

from WebDetect.network import Network


class ImageHandler:
    #one option goes to one network
    def __init__(self, json_options, image):
        self.thread_list = list()
        options = self.retrieve_options_from_json(json_options)

        for option in options:
            new_thread = NetworkThread(option, image)
            new_thread.start()
            self.thread_list.append(new_thread)

    def get_answer(self):
        alive = True
        while alive:
            alive = False
            for net in self.thread_list:
                if net.is_alive():
                    alive = True
                    break
        answer_list = list()
        for net in self.thread_list:
            answer_list.append(net.answer)
        return self.make_answers_json(answer_list)

    @staticmethod
    def retrieve_options_from_json(json_options):
        prepare = json_options.split('\n')
        prepare = ''.join(prepare)
        prepare = json.loads(prepare)
        return prepare['options']

    @staticmethod
    def make_answers_json(answers):
        y = dict()
        y['answers'] = answers
        return json.dumps(y)


class NetworkThread(Thread):
    def __init__(self, option, image):
        Thread.__init__(self)
        self.option = option
        self.image = image
        self.answer = 0

    def run(self):
        mynet = Network(self.option, self.image)
        self.answer = mynet.answer


if __name__ == '__main__':
    # test code

    s = ""
    with open("options.json", "r") as file:
        for line in file:
            s += line
    s = s.split('\n')
    s = ''.join(s)

    handler = ImageHandler(s, 1)

    print(handler.get_answer())
