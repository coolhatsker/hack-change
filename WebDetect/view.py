from django.http import HttpResponse
from django.views.generic.base import View
from WebDetect.image_handler import ImageHandler
from PIL import Image
import time
import json
from threading import Thread


class ImageLoader(View):
    def dispatch(self, request, *args, **kwargs):
        config = json.loads(ImageHandlerThread.file_as_string("./WebDetect/config.json"))
        handlers = list()
        for i in range(1, config["cameras"] + 1):
            spconfig = config.copy()
            spconfig["output_name"] += str(i)
            spconfig["output_json"] += str(i)
            spconfig["input_dir"] += str(i) + "/"
            handler_thread = ImageHandlerThread(spconfig)
            handlers.append(handler_thread)
            handler_thread.start()
        alive = True
        while alive:
            alive = False
            for mythread in handlers:
                if mythread.is_alive():
                    alive = True
                    break
        return HttpResponse("Execution ended")


class ImageHandlerThread(Thread):
    def __init__(self, config):
        Thread.__init__()
        self.config = config

    def run(self):
        for open_img in self.image_to_open():
            try:
                img = Image.open(open_img)
                img.save(self.config["output_name"] + self.config["format"])

                json_options = self.file_as_string(self.config["options"])
                handler = ImageHandler(json_options, img)
                answers = handler.get_answer()
                file = open(self.config["output_json"] + ".json", "w+")
                file.write(answers)
                time.sleep(self.config["delay"])
            except IOError:
                print("kke")

    @staticmethod
    def file_as_string(filename):
        with open(filename, "r") as file:
            data = file.read().replace('\n', '')
        return data

    def image_to_open(self):
        ptrn = self.config["input_dir"] + self.config["input"]
        frmt = self.config["format"]
        index = 0
        while index < 1:
            img_name = ptrn + str(index) + frmt
            yield img_name
            index += 1
